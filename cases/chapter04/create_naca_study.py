#!/usr/bin/env python 

import argparse
import sys
from subprocess import call

parser = argparse.ArgumentParser(description='Runs pyFoamRunParameterVariation.py to create simulation folders without generating the mesh.')

parser.add_argument('--study_name', dest="study_name", type=str, required=True,
                    help='Name of the parameter study.')

parser.add_argument('--parameter_file', dest="parameter_file", type=str, required=True,
                    help='PyFoam .parameter file')

parser.add_argument('--template_case', dest="template_case", type=str, required=True, 
                    help='Parameter study template case.') 

args = parser.parse_args()

if __name__ == '__main__':

    args = parser.parse_args(sys.argv[1:])

    prefix = args.study_name + "_" + args.parameter_file

    call_args = ["pyFoamRunParameterVariation.py", 
                 "--every-variant-one-case-execution", 
                 "--create-database", 
                 "--no-mesh-create",
                 "--no-server-process",
                 "--no-execute-solver",
                 "--no-case-setup",
                 "--cloned-case-prefix=%s" % prefix,
                 args.template_case, 
                 args.parameter_file]

    call(call_args)
